# Slides on Programming Topics

This repository contains slides and additional examples/material regarding various
topics related to programming, algorithms, data structures etc.
These are meant to be used in my courses/lectures.

My courses are held in german using different programming languages.
Thus, there will be german text and more than one programming language will be used
in examples.
